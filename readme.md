# Lizard SATD plugin

Provides a simple Self Admitted Technical Debt (SATD) plugin for [Lizard](https://github.com/terryyin/lizard).
It counts SATD comments in methods.

## Installation

Copy the lizardsatd.py file to the directory of your script.

Import it in your script after lizard:
```python
import lizard
import lizardsatd
```

Use it as extension:
```python
analyze_file = lizard.FileAnalyzer(lizard.get_extensions([lizardsatd.LizardExtension()]))
r = analyze_file('/tmp/test.java')
```
